package com.fanghuaiming.service;

import com.fanghuaiming.dao.DictMapper;
import com.fanghuaiming.dao.XaMapper;
import org.apache.shardingsphere.transaction.annotation.ShardingTransactionType;
import org.apache.shardingsphere.transaction.core.TransactionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/****
 * @description:
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/23 下午3:42
 *
 */
@Service
public class XaService {

    @Autowired
    private XaMapper xaMapper;

    @Autowired
    private DictMapper dictMapper;

    @Transactional(rollbackFor = Exception.class)
    @ShardingTransactionType(TransactionType.XA)
    //测试本地事务并不会回滚
//    @ShardingTransactionType(TransactionType.LOCAL)
    public void insertXa(String value,int i){
        dictMapper.insertDict(13L,"user_type3","3","管理员3");
        if(i == 2){
            i = i /0;
        }
        xaMapper.insertXa(value);
    }

}
