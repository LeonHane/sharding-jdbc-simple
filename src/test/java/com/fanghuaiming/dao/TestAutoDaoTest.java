package com.fanghuaiming.dao;

import com.fanghuaiming.ShardingJdbcSimpleApplication;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/****
 * @description:
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/23 下午1:57
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ShardingJdbcSimpleApplication.class})
public class TestAutoDaoTest {

    @Autowired
    TestAutoDao testAutoDao;

    /**
     * 垂直拆库插入
     */
    @Test
    public void testInsertTest(){
        for (int i = 0 ; i<10; i++){
            Long id = i + 1L; testAutoDao.insertTestAuto("姓名"+ id );
        }
    }

    /**
     * 垂直拆库查询
     */
    @Test
    public void testSelectTestbyIds(){
        List<Long> testAutoIds = new ArrayList<>();
        testAutoIds.add(1L);
        testAutoIds.add(2L);
        List<Map> users = testAutoDao.selectTestAutobyIds(testAutoIds);
        System.out.println(users);
    }

}
