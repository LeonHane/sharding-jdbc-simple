package com.fanghuaiming.dao;

import com.fanghuaiming.ShardingJdbcSimpleApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/****
 * @description:
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/23 下午3:16
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ShardingJdbcSimpleApplication.class})
public class DictTest {

    @Autowired
    DictMapper dictDao;

    @Test
    public void testInsertDict(){
        dictDao.insertDict(3L,"user_type3","3","管理员3");
        dictDao.insertDict(4L,"user_type4","4","操作员4");
    }

    @Test public void testDeleteDict(){
        dictDao.deleteDict(1L);
        dictDao.deleteDict(2L);
    }

}
