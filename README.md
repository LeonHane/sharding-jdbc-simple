# sharding-jdbc-simple

#### 介绍
Sharding-jdbc整合

- 2020-11-20:
1. 初始化整合,配置文件根据sharding-sphere文档而来,本项目使用4.x版本.
2. 水平分表

- 2020-11-23:
1. 水平分库
2. 水平分库下水平分表
3. 垂直分库
4. 广播表(测试默认没有分布式事务)
5. XA-2PC二阶段提交分布式事务(Atomikos)
6. Seata-BASE-AT柔性事务

- 2020-11-24
1. Sentinel限流(直接、排队、预热)
2. Jemeter测试用例(Sentinel.jmx)
