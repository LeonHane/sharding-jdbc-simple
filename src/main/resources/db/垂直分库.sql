/*
 Navicat Premium Data Transfer

 Source Server         : localMySql
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3306
 Source Schema         : order_db

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 23/11/2020 14:42:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_order_1
-- ----------------------------
DROP TABLE IF EXISTS `t_order_1`;
CREATE TABLE `t_order_1` (
  `order_id` bigint(20) NOT NULL COMMENT '订单id',
  `price` decimal(10,2) NOT NULL COMMENT '订单价格',
  `user_id` bigint(20) NOT NULL COMMENT '下单用户id',
  `status` varchar(50) NOT NULL COMMENT '订单状态',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for t_order_2
-- ----------------------------
DROP TABLE IF EXISTS `t_order_2`;
CREATE TABLE `t_order_2` (
  `order_id` bigint(20) NOT NULL COMMENT '订单id',
  `price` decimal(10,2) NOT NULL COMMENT '订单价格',
  `user_id` bigint(20) NOT NULL COMMENT '下单用户id',
  `status` varchar(50) NOT NULL COMMENT '订单状态',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for t_test_auto
-- ----------------------------
DROP TABLE IF EXISTS `t_test_auto`;
CREATE TABLE `t_test_auto` (
  `test_auto_id` bigint(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`test_auto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_test_snow
-- ----------------------------
DROP TABLE IF EXISTS `t_test_snow`;
CREATE TABLE `t_test_snow` (
  `test_snow_id` bigint(50) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`test_snow_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_test_snow
-- ----------------------------
BEGIN;
INSERT INTO `t_test_snow` VALUES (537640566213574657, '姓名1');
INSERT INTO `t_test_snow` VALUES (537640566435872768, '姓名2');
INSERT INTO `t_test_snow` VALUES (537640566448455681, '姓名3');
INSERT INTO `t_test_snow` VALUES (537640566461038592, '姓名4');
INSERT INTO `t_test_snow` VALUES (537640566473621505, '姓名5');
INSERT INTO `t_test_snow` VALUES (537640566490398720, '姓名6');
INSERT INTO `t_test_snow` VALUES (537640566502981633, '姓名7');
INSERT INTO `t_test_snow` VALUES (537640566515564544, '姓名8');
INSERT INTO `t_test_snow` VALUES (537640566532341761, '姓名9');
INSERT INTO `t_test_snow` VALUES (537640566544924672, '姓名10');
INSERT INTO `t_test_snow` VALUES (537640734786846721, '姓名1');
INSERT INTO `t_test_snow` VALUES (537640734971396096, '姓名2');
INSERT INTO `t_test_snow` VALUES (537640734979784705, '姓名3');
INSERT INTO `t_test_snow` VALUES (537640734992367616, '姓名4');
INSERT INTO `t_test_snow` VALUES (537640735004950529, '姓名5');
INSERT INTO `t_test_snow` VALUES (537640735017533440, '姓名6');
INSERT INTO `t_test_snow` VALUES (537640735030116353, '姓名7');
INSERT INTO `t_test_snow` VALUES (537640735038504960, '姓名8');
INSERT INTO `t_test_snow` VALUES (537640735051087873, '姓名9');
INSERT INTO `t_test_snow` VALUES (537640735059476480, '姓名10');
COMMIT;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `fullname` varchar(255) NOT NULL COMMENT '用户姓名',
  `user_type` char(1) DEFAULT NULL COMMENT '用户类型',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_user
-- ----------------------------
BEGIN;
INSERT INTO `t_user` VALUES (1, '姓名1', NULL);
INSERT INTO `t_user` VALUES (2, '姓名2', NULL);
INSERT INTO `t_user` VALUES (3, '姓名3', NULL);
INSERT INTO `t_user` VALUES (4, '姓名4', NULL);
INSERT INTO `t_user` VALUES (5, '姓名5', NULL);
INSERT INTO `t_user` VALUES (6, '姓名6', NULL);
INSERT INTO `t_user` VALUES (7, '姓名7', NULL);
INSERT INTO `t_user` VALUES (8, '姓名8', NULL);
INSERT INTO `t_user` VALUES (9, '姓名9', NULL);
INSERT INTO `t_user` VALUES (10, '姓名10', NULL);
INSERT INTO `t_user` VALUES (11, '姓名11', NULL);
INSERT INTO `t_user` VALUES (12, '姓名12', NULL);
INSERT INTO `t_user` VALUES (13, '姓名13', NULL);
INSERT INTO `t_user` VALUES (14, '姓名14', NULL);
INSERT INTO `t_user` VALUES (15, '姓名15', NULL);
INSERT INTO `t_user` VALUES (16, '姓名16', NULL);
INSERT INTO `t_user` VALUES (17, '姓名17', NULL);
INSERT INTO `t_user` VALUES (18, '姓名18', NULL);
INSERT INTO `t_user` VALUES (19, '姓名19', NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
