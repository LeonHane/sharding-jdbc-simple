package com.fanghuaiming.dao;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface TestAutoDao {

    @Insert("insert into t_test_auto(name) value(#{name})")
    int insertTestAuto(@Param("name") String name);

    @Select({"<script>"+
            " select"+
            " * "+
            " from t_test_auto t "+
            " where t.test_auto_id in"+
            "<foreach collection='testAutoIds' item='testAutoId' open='(' separator=',' close=')'>"+
            "#{testAutoId}"+
            "</foreach>"+
            "</script>" })
    List<Map> selectTestAutobyIds(@Param("testAutoIds") List<Long> testAutoIds);
}