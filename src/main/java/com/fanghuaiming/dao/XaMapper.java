package com.fanghuaiming.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface XaMapper {

    @Insert("insert into t_xa(name) value(#{name})")
    int insertXa(@Param("name") String name);


    @Delete("delete from t_xa where id = #{id}")
    int deleteXa(@Param("id") Long id); }