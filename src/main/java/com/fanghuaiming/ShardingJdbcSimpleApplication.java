package com.fanghuaiming;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/****
 * @description:
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/20 上午11:04
 *
 */
@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = "com.fanghuaiming")
@ComponentScan(basePackages = {"com.fanghuaiming.**"})
@MapperScan(basePackages = "com.fanghuaiming.dao")
public class ShardingJdbcSimpleApplication {

  public static void main(String[] args) {
      SpringApplication.run(ShardingJdbcSimpleApplication.class,args);
  }
}
