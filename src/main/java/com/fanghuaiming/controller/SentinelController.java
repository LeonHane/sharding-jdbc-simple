package com.fanghuaiming.controller;

import com.fanghuaiming.service.SentinelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/****
 * @description:
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/24 下午1:50
 *
 */
@RestController
public class SentinelController {

    @Autowired
    private SentinelService sentinelService;

    /**
     * @Description: Get请求有时候有缓存换成post结合jemeter方便测试
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/11/24 下午2:50
     * @version:1.0.0
     */
    @GetMapping(value = "/loadSentinel")
    public String loadSentinel(@RequestParam String name) {
        return sentinelService.loadSentinel(name);
    }

    /**
     * @Description: Warm Up
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/11/24 下午3:09
     * @version:1.0.0
     */
    @GetMapping(value = "/warmUp")
    public String warmUp() {
        return sentinelService.warmUp();
    }

    /**
     * @Description: BlockQueue
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/11/24 下午3:15
     * @version:1.0.0
     */
    @GetMapping(value = "/blockQueue")
    public String blockQueue() {
        return sentinelService.blockQueue();
    }
}
