package com.fanghuaiming.dao;

import com.fanghuaiming.ShardingJdbcSimpleApplication;
import com.fanghuaiming.service.XaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/****
 * @description:
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/23 下午4:16
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ShardingJdbcSimpleApplication.class})
public class TestXATransactionDictTest {

    @Autowired
    XaService service;

    @Test
    public void testInsertDict(){
        service.insertXa("name8",2);
    }

}
