/*
 Navicat Premium Data Transfer

 Source Server         : localMySql
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3306
 Source Schema         : order_db

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 20/11/2020 16:33:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_order_1
-- ----------------------------
DROP TABLE IF EXISTS `t_order_1`;
CREATE TABLE `t_order_1` (
  `order_id` bigint(20) NOT NULL COMMENT '订单id',
  `price` decimal(10,2) NOT NULL COMMENT '订单价格',
  `user_id` bigint(20) NOT NULL COMMENT '下单用户id',
  `status` varchar(50) NOT NULL COMMENT '订单状态',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_order_1
-- ----------------------------
BEGIN;
INSERT INTO `t_order_1` VALUES (536579992096604160, 2.00, 1, 'SUCCESS');
INSERT INTO `t_order_1` VALUES (536579992121769984, 4.00, 1, 'SUCCESS');
INSERT INTO `t_order_1` VALUES (536579992155324416, 6.00, 1, 'SUCCESS');
INSERT INTO `t_order_1` VALUES (536579992184684544, 8.00, 1, 'SUCCESS');
INSERT INTO `t_order_1` VALUES (536579992209850368, 10.00, 1, 'SUCCESS');
INSERT INTO `t_order_1` VALUES (536579992235016192, 12.00, 1, 'SUCCESS');
INSERT INTO `t_order_1` VALUES (536579992260182016, 14.00, 1, 'SUCCESS');
INSERT INTO `t_order_1` VALUES (536579992297930752, 16.00, 1, 'SUCCESS');
INSERT INTO `t_order_1` VALUES (536579992331485184, 18.00, 1, 'SUCCESS');
COMMIT;

-- ----------------------------
-- Table structure for t_order_2
-- ----------------------------
DROP TABLE IF EXISTS `t_order_2`;
CREATE TABLE `t_order_2` (
  `order_id` bigint(20) NOT NULL COMMENT '订单id',
  `price` decimal(10,2) NOT NULL COMMENT '订单价格',
  `user_id` bigint(20) NOT NULL COMMENT '下单用户id',
  `status` varchar(50) NOT NULL COMMENT '订单状态',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_order_2
-- ----------------------------
BEGIN;
INSERT INTO `t_order_2` VALUES (536579991593287681, 1.00, 1, 'SUCCESS');
INSERT INTO `t_order_2` VALUES (536579992109187073, 3.00, 1, 'SUCCESS');
INSERT INTO `t_order_2` VALUES (536579992138547201, 5.00, 1, 'SUCCESS');
INSERT INTO `t_order_2` VALUES (536579992167907329, 7.00, 1, 'SUCCESS');
INSERT INTO `t_order_2` VALUES (536579992201461761, 9.00, 1, 'SUCCESS');
INSERT INTO `t_order_2` VALUES (536579992222433281, 11.00, 1, 'SUCCESS');
INSERT INTO `t_order_2` VALUES (536579992247599105, 13.00, 1, 'SUCCESS');
INSERT INTO `t_order_2` VALUES (536579992276959233, 15.00, 1, 'SUCCESS');
INSERT INTO `t_order_2` VALUES (536579992323096577, 17.00, 1, 'SUCCESS');
INSERT INTO `t_order_2` VALUES (536579992344068097, 19.00, 1, 'SUCCESS');
INSERT INTO `t_order_2` VALUES (536582701860257793, 5.00, 1, 'SUCCESS');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
