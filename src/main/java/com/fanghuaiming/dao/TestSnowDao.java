package com.fanghuaiming.dao;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface TestSnowDao {

    @Insert("insert into t_test_snow(name) value(#{name})")
    int insertTestSnow( @Param("name") String name);

    @Select({"<script>"+
            " select"+
            " * "+
            " from t_test_snow t "+
            " where t.test_snow_id in"+
            "<foreach collection='testSnowIds' item='testSnowId' open='(' separator=',' close=')'>"+
            "#{testSnowId}"+
            "</foreach>"+
            "</script>" })
    List<Map> selectTestSnowbyIds(@Param("testSnowIds") List<Long> testSnowIds);
}