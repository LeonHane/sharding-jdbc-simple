package com.fanghuaiming.dao;

import com.fanghuaiming.ShardingJdbcSimpleApplication;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Administrator
 * @version 1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ShardingJdbcSimpleApplication.class})
public class OrderDaoTest {

    @Autowired
    OrderDao orderDao;

    @Test
    public void testInsertOrderfenbiao(){
            orderDao.insertOrder(new BigDecimal(5),1L,"SUCCESS");
    }

    @Test
    public void testSelectOrderbyIdsfenbiao(){
        List<Long> ids = new ArrayList<Long>();
        ids.add(536579992235016192L);
        ids.add(536579992209850368L);

        List<Map> maps = orderDao.selectOrderbyIds(ids);
        System.out.println(maps);
    }

    @Test public void testInsertOrderfenku(){
        /*for (int i = 0 ; i<10; i++){
            orderDao.insertOrder(new BigDecimal((i+1)*5),1L,"WAIT_PAY");
        }for (int i = 0 ; i<10; i++){
            orderDao.insertOrder(new BigDecimal((i+1)*10),2L,"WAIT_PAY");
        }*/
        for (int i = 0 ; i<10; i++){
            orderDao.insertOrder(new BigDecimal((i+1)*10),2L,"WAIT_PAY");
        }
    }

    @Test
    public void testSelectOrderbyIdsfenku(){
        List<Long> ids = new ArrayList<Long>();
        ids.add(537597487561572352L);
        ids.add(537599225240748033L);

        List<Map> maps = orderDao.selectOrderbyIds(ids);
        System.out.println(maps);
    }

    @Test public void testSelectOrderbyUserAndIds(){
    List<Long> orderIds = new ArrayList<>();
    //order1库order2表
    orderIds.add(537598878191452161L);
    //order1库order1表
    orderIds.add(537599225202999296L);
    //查询条件中包括分库的键user_id
    int user_id = 2;
    List<Map> orders = orderDao.selectOrderbyUserAndIds(user_id,orderIds);
    JSONArray jsonOrders = new JSONArray(orders);
    System.out.println(jsonOrders);
    }
}
